require 'spec_helper'
require_relative '../cat'

describe Cat do
  describe '#meow' do
    subject { described_class.new.meow }

    it { is_expected.to eq('meows') }
    it { is_expected.to eq('meowzz') }
  end
end
